
**Mittlerweile liegt der Koalitionsvertrag zwischen der SPD und Bündnis 90/Die Grünen
vor. Das Code for Hamburg Lab (CfHH) hat einmal nachgeschaut und die relevanten
Punkte hinsichtlich Open-Data, Open-Source und Transparenz anzuschauen und zu
kommentieren.**

**Kurzfassung:**

HmbTG beibehalten;
Transparenz in Verfassung niederschreiben;
Hamburgischer Beauftragter für Datenschutz und Informationsfreiheit (HmbBfDI) soll
bedarfsgerecht ausgestattet werden;
Deputationen abschaffen und die Verwaltung durch Verfassungsauftrag transparenter machen;
Parlamentsdatenbank soll offene Schnittstelle für maschinenlesbare Daten bekommen;
Track & Trace Verfahren weiter ausbauen;
Transparenz von Algorithmen;
Transparenz von Klimadaten;
Einführung Daten-Governance, Buildung Information Modeling und Digitaler Zwilling;
Mehr Einsatz von Open Source, interne Fachanwendungen sollen quellenoffen sein;

**Langfassung:**

Mit der Einführung eines Transparenzgesetzes war Hamburg 2012 bundesweit hinsichtlich
seiner weitreichenden Transparenz führend. Maßgeblich ging die Einführung auf ein Bündnis
bestehend aus (Transparency International Deutschland, Chaos Computer Club, Mehr
Demokratie) zurück, welche eine Volksinitiative „Transparenz schafft Vertrauen“ dies in den
Gesetzgebungsprozess 2011 eingebracht hat. Die letzte Änderung des Hamburgischen
Transparenzgesetz (HmbTG) erfolgte 2019, dieser Status soll laut Koalitionsvertrag
beibehalten werden. Vor der Änderung erfolgte 2017 eine externe Evaluation des Gesetzes,
welches eine hohe Nutzung innerhalb der Hamburger Behörden bescheinigte. Zusätzlich soll
jedoch das Rechtsinstrument des Transparenzgesetzes mit einer Verfassungsgarantie
abgesichert werden. Der Beauftragte für den Datenschutz und die Informationsfreiheit
(HmBfDI) soll bedarfsgerecht für den Bereich Datenschutz als auch Informationsfreiheit
ausgestattet werden.

Die Deputationen (besondere Bürger*innengremien) sollen abgeschafft werden. Die
Verwaltung soll daher in einem Verfassungsauftrag für eine Bürger*innennahe und
transparente Verwaltung hin weiterentwickelt werden.

Die Parlamentsdatenbank, welche die Drucksachen enthält, soll um eine offene Schnittstelle
für maschinenlesbare Inhalte ergänzt werden, ähnlich wie beim Transparenzportal.

Eine bessere Nachverfolgbarkeit und Transparenz sollen das digitale Track & Trace-Verfahren
für Unternehmen zum jeweiligen Stand von Genehmigungsverfahren bieten. Hierbei wurden
zuerst die immissionsschutzrechtlichen Genehmigungsverfahren bereits digitalisiert, künftig
sollen auch Baugenehmigungsverfahren in Hamburger Hafen darunterfallen.

Hamburg soll sich auf nationaler und europäischer Ebene unter Berücksichtigung der
Länderkompetenzen für Medien und Vielfaltsicherung für die Schaffung von gesetzlichen
Regelungen einsetzen, durch welche die Verwender*innen von Algorithmen zu mehr
Transparenz (hierbei zur Offenlegung der wesentlichen Kriterien, die in den Algorithmus
einfließen) verpflichtet werden. Dies soll unter Berücksichtigung der Aspekte der Meinungs-
und Angebotsvielfalt erfolgen. Künstliche Intelligenz wird als Möglichkeit zur
Modernisierung und Beschleunigung von Verwaltungsabläufen verstanden. Eine
entsprechende Transparenz von KI-Anwendungen soll ermöglicht werden.

Die Transparenz für Klimadaten soll erhöht werden, dabei soll verstärkt zentral erfasst,
aufbereitet und der Öffentlich, den Behörden und anderen Stakeholdern zur Verfügung gestellt
werden.

Es soll ein Daten-Governance eingeführt werden, damit ein stadtweites Verständnis sowie
geeignete Regelungen und Mechanismen entstehen, um diese fach- und
institutionenübergreifende Datennutzung effizient zu realisieren. Um Daten besser verfügbar
zu machen, sollen diese in die Urban Data Plattform einfließen. Diese Plattform soll technisch
weiter verbessert werden, um die verschiedenen Datenbanken und Systeme miteinander zu
verbinden. Hierzu gehört auch die Einführung der digitalen Arbeitsmethodik „Building
Information Modeling“ (BIM) wo alle am Bau eines Bauwerks beteiligten öffentlichen
Einrichtungen relevante Daten in ein intelligentes Bauwerksinformationsmodell (Digitaler
Zwilling) einfügen. Das Geoportal soll weiter ausgebaut werden und weitere Informationen
enthalten.

Die Hansestadt Hamburg wird verstärkt auf den Einsatz von Open Source Produkte setzen.
Dabei sollen viele Fachanwendungen, die bisher entwickelt werden, quellenoffen sein und
zwischen den Verwaltungen weitergabefähig. Die Stadt will Initiativen entwickeln, bei
lizenzgeschützter Software, die für die öffentliche Verwaltung als Kundin auch hier zu Open
Source vergleichbare Transparenz herstellt.

**Bewertung einiger Punkte:**

Im bundesweiten Ranking (LINK) für Informationsfreiheit und Transparenz belegt zwar das
HmbTG den ersten Platz, aber mit nur 66 % der möglichen Punktzahl. Weiterhin bleibt
problematisch, dass keine anonymen Anträge möglich sind. Die Antwortfristen wurden
deutlich verlängert. Weiterhin gibt es noch zahlreiche Ausnahmen wie die Hochschulen,
Geheimdienste, Rundfunk, Rechnungshof, Sparkassen und Landesbank sowie Gerichte,
Strafverfolgungs- und Strafvollstreckungsbehörden. Leider zeigt sich auch bei den
Ausnahmen, dass die Abwägungen mit öffentlichem Interesse mangelhaft sind. Eine
Gebührenfreiheit ist nur sehr eingeschränkt möglich, genauso wie die Sanktionen bei
Nichteinhaltung der Fristen. Weiterhin bleibt der Beauftragte für den Datenschutz und
Informationsfreiheit ein zu großen Teilen zahnloser Tiger, da dieser keinerlei Klagerecht oder
größere Sanktionsmöglichkeiten besitzt.

Schließlich bleibt festzuhalten, dass bisher die Arbeitsbelastung rund um Datenschutz in den
letzten Jahren seit Einführung der DSGVO und der Digitalisierung massiv für den
Beauftragten für den Datenschutz und Informationsfreiheit zugenommen hat. Auch war seit
Einführung des HmbTG nicht zu erkennen, dass entsprechend die Personal-Stellen erhöht
worden sind. Daher ist ein Ausbau in beiden Bereichen zu begrüßen, wenn gleich die
formalen Restriktionsmöglichkeiten und Klagerechte hinsichtlich Transparenzverstößen
begrenzt ist.

Künftig sollen das Geoportal, Klimaportal und die Parlamentsdatenbank ausgebaut werden
bzw. letztes maschinenlesbar gemacht werden. Auch die Umsetzung von mehr Schnittstellen,
ist auch der Wunsch von CfHH. Somit wird begrüßt, dass diese Portale künftig offener gestaltet
werden und mehr Inhalte erhalten.

Im Grundsatz begrüßt CfHH die Einführung eines Daten-Governance und entsprechende
Bereitstellung in der Urban Data Plattform. Hierbei können sich ganz neue Anwendungsfelder
ergeben und neue Innovationen. Schließlich bleibt zu sagen, dass der vermehrte Einsatz von
Open-Source der richtige Weg ist, genauso die die Transparenz von Algorithmen. Gleichzeitig
sollte aber ein Weg gewählt werden offene Standards auch entsprechend in vorhandene und
neue Applikationen umzusetzen.

Transparenzgesetz (S. 147)

Transparenzportal (S. 160)

Track & Trace (S. 44)

Algorithmen (S. 61, 163)

Klimadaten (S. 65)

Open-Source (S. 161)

Quellen: 
https://www.transparenzranking.de