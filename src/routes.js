import Main from './components/Main.vue';
import Team from './components/Team.vue';
import Impressum from './components/Impressum.vue';
import Satzung from './components/Satzung.vue';
import Blog from './components/Blog.vue';

import BlogEntries  from './data/blogs.json'

const blogRoutes = Object.keys(BlogEntries).map(section => {
    const children = BlogEntries[section].map(child => ({
      path: child.id,
      name: child.id,
      component: () => import(`./markdowns/${section}/${child.id}.md`)
    }))
    return {
      path: `/${section}`,
      name: section,
      component: () => import('./components/BlogEntry.vue'),
      children
    }
  })

const routes = [
    { path: '/', component: Main},
    { path: '/team', component: Team },
    { path: '/impressum', component: Impressum },
    { path: '/satzung', component: Satzung },
    { path: '/blog', component: Blog },
    ...blogRoutes
];

export default routes;